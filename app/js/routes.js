angular.module("app").config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$breadcrumbProvider",
    function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $urlRouterProvider.otherwise("/dashboard");
        $ocLazyLoadProvider.config({
            debug: false
        });
        $breadcrumbProvider.setOptions({
            prefixStateName: "app.main",
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });
        $stateProvider.state("app", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Root",
                skip: true
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ],
            }
        }).state("app.main", {
            url: "/dashboard",
            templateUrl: "tpl/dashboard/dashboard.html",
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/dashboard/dashboard.js"]
                            });
                        });
                    }
                ]
            }
        }).state("app.generator", {
            url: "/generator",
            templateUrl: "tpl/generator/index.html",
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/generator/index.js"]
                            });
                        });
                    }
                ]
            }
        }).state("app.pemasukan", {
            url: "/pemasukan",
            templateUrl: "api/vendor/cahkampung/landa-acc/tpl/t_pemasukan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["api/vendor/cahkampung/landa-acc/tpl/t_pemasukan/index.js"]
                        });
                    }
                ]
            }
        }).state("master", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Master"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
                authenticate: authenticate
            }
        }).state("master.item", {
            url: "/masteritem",
            templateUrl: "tpl/item/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/item/index.js"]
                        });
                    }
                ]
            }
        }).state("master.mahasiswa", {
            url: "/m_mahasiswa",
            templateUrl: "tpl/m_mahasiswa/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_mahasiswa/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_kelas", {
            url: "/m_kelas",
            templateUrl: "tpl/m_kelas/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_kelas/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_jurusan", {
            url: "/m_jurusan",
            templateUrl: "tpl/m_jurusan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_jurusan/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_sekolah", {
            url: "/m_sekolah",
            templateUrl: "tpl/m_sekolah/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_sekolah/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_kategori", {
            url: "/m_kategori",
            templateUrl: "tpl/m_kategori/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_kategori/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_kategori_artikel", {
            url: "/m_kategori_artikel",
            templateUrl: "tpl/m_kategori_artikel/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_kategori_artikel/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_artikel", {
            url: "/m_artikel",
            templateUrl: "tpl/m_artikel/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_artikel/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_barang", {
            url: "/m_barang",
            templateUrl: "tpl/m_barang/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        // return $ocLazyLoad.load(['angularFileUpload']).then(() => {
                        //     return $ocLazyLoad.load({
                        //         files: ["tpl/m_barang/index.js"]
                        //     });
                        // });
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_barang/index.js"]
                            });
                        });
                    }
                ]
            }
        }).state("master.m_customer", {
            url: "/m_customer",
            templateUrl: "tpl/m_customer/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_customer/index.js"]
                        });
                    }
                ]
            }
        }).state("master.m_supplier", {
            url: "/m_supplier",
            templateUrl: "tpl/m_supplier/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_supplier/index.js"]
                        });
                    }
                ]
            }
        }).state("master.penjualan", {
            url: "/penjualan",
            templateUrl: "tpl/penjualan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/penjualan/index.js"]
                        });
                    }
                ]
            }
        }).state("transaksi", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Pengelompokan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
                authenticate: authenticate
            }
        }).state("transaksi.t_kelompok_kelas", {
            url: "/t_kelompok_kelas",
            templateUrl: "tpl/t_kelompok_kelas/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_kelompok_kelas/index.js"]
                        });
                    }
                ]
            }
        }).state("transaksi.t_penjualan", {
            url: "/t_penjualan",
            templateUrl: "tpl/t_penjualan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_penjualan/index.js"]
                        });
                    }
                ]
            }
        }).state("transaksi.t_pembelian", {
            url: "/t_pembelian",
            templateUrl: "tpl/t_pembelian/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_pembelian/index.js"]
                        });
                    }
                ]
            }
        }).state("laporan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Laporan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
                authenticate: authenticate
            }
        }).state("laporan.l_mahasiswa", {
            url: "/l_mahasiswa",
            templateUrl: "tpl/l_mahasiswa/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_mahasiswa/index.js"]
                        });
                    }
                ]
            }
        }).state("laporan.l_jurusan", {
            url: "/l_jurusan",
            templateUrl: "tpl/l_jurusan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_jurusan/index.js"]
                        });
                    }
                ]
            }
        }).state("laporan.l_siswa", {
            url: "/l_siswa",
            templateUrl: "tpl/l_siswa/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_siswa/index.js"]
                        });
                    }
                ]
            }
        }).state("laporan.l_penjualan", {
            url: "/l_penjualan",
            templateUrl: "tpl/l_penjualan/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_penjualan/index.js"]
                        });
                    }
                ]
            }
        }).state("laporan.l_penjualanbarang", {
            url: "/l_penjualanbarang",
            templateUrl: "tpl/l_penjualanbarang/index.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_penjualanbarang/index.js"]
                        });
                    }
                ]
            }
        }).state("pengguna", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "User Login"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
                authenticate: authenticate
            }
        }).state("pengguna.akses", {
            url: "/hak-akses",
            templateUrl: "tpl/m_akses/index.html",
            ncyBreadcrumb: {
                label: "Hak Akses"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_akses/index.js"]
                        });
                    }
                ]
            }
        }).state("pengguna.user", {
            url: "/user",
            templateUrl: "tpl/m_user/index.html",
            ncyBreadcrumb: {
                label: "Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/index.js"]
                        });
                    }
                ]
            }
        }).state("pengguna.profil", {
            url: "/profil",
            templateUrl: "tpl/m_user/profile.html",
            ncyBreadcrumb: {
                label: "Profil Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/profile.js"]
                        });
                    }
                ]
            }
        }).state("page", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html",
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ]
            }
        }).state("page.login", {
            url: "/login",
            templateUrl: "tpl/common/pages/login.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/login.js"]
                        });
                    }
                ]
            }
        }).state("page.404", {
            url: "/404",
            templateUrl: "tpl/common/pages/404.html"
        }).state("page.500", {
            url: "/500",
            templateUrl: "tpl/common/pages/500.html"
        });

        function authenticate($q, UserService, $state, $transitions, $location, $rootScope) {
            var deferred = $q.defer();
            if (UserService.isAuth()) {
                deferred.resolve();
                var fromState = $state;
                var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator"];
                $transitions.onStart({}, function ($transition$) {
                    var toState = $transition$.$to();
                    if ($rootScope.user.akses[toState.name.replace(".", "_")] || globalmenu.indexOf(toState.name)) {
                    } else {
                        $state.target("page.500")
                    }
                });
            } else {
                $location.path("/login");
            }
            return deferred.promise;
        }
    }
]);