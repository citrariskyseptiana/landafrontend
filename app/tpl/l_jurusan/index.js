 app.controller("ljurusanCtrl", function ($scope, Data, $rootScope) {
    $scope.filter = {};
    $scope.tampilkan = false;

    $scope.getLaporan = function (filter) {
        console.log(filter);
        Data.get("l_jurusan/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.listMhs = data.data.list;
                console.log($scope.listMhs);
                console.log(data);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };
    Data.get("l_jurusan/jurusan").then(function (data) {
        if (data.status_code == 200) {
            $scope.listJurusan = data.data.list;
            console.log($scope.listJurusan);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });

    $scope.getListKelas = function (m_jurusan_id) {
        Data.get("l_jurusan/kelas/"+ m_jurusan_id).then(function (data) {
            if (data.status_code == 200) {
                $scope.listKelas = data.data.list;
                console.log($scope.listKelas);
                console.log(data);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }

        });
    }
});