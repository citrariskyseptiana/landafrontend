app.controller("lpenjualanbarangCtrl", function ($scope, Data, $rootScope) {
    $scope.filter = {};
    $scope.tampilkan = false;
    $scope.filter.bulan = new Date();
    // $scope.form = {
    //     jumlahbrg : 0,
    //     total : 0
    // }

    $scope.getLaporan = function (filter) {
        console.log(filter);
        Data.get("l_penjualanbarang/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.listPenjualanBarang = data.data.list;
                console.log($scope.listPenjualanBarang);
                console.log(data);
                $scope.TotalS = data.data.totalsemua;
                $scope.Rincian = data.data.rincian;
                //=====================Fungsi menghitung pada js==========================
                // $scope.getSubTotal();
                // $scope.JumlahBarang = data.data.jumlah_barang;
                // $scope.TotalHarga = data.data.total_harga;
                // console.log(JumlahBarang);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });
    };
    // $scope.getSubTotal = function () { //fungsi yang ketika input jumlah dan harga subtotal dan total akan otomatis terisi
    //     var total = 0;
    //     var jumlahbrg = 0;
    //     angular.forEach($scope.listPenjualanBarang, function (value, key) {
    //         value.SubTotal = value.jumlah_barang * value.harga_satuan;
    //         total = total + value.SubTotal;
    //         jumlahbrg = jumlahbrg + value.jumlah_barang;
    //     });
    //     console.log(jumlahbrg);
    //     console.log(total);
    //     $scope.form.total = total;
    //     $scope.form.jumlahbrg = jumlahbrg;
    // };
    Data.get("l_penjualanbarang/barang").then(function (data) {
        if (data.status_code == 200) {
            $scope.listBarang = data.data.list;
            console.log($scope.listBarang);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });
    Data.get("l_penjualanbarang/customer").then(function (data) {
        if (data.status_code == 200) {
            $scope.listCustomer = data.data.list;
            console.log($scope.listCustomer);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });

    $scope.view = function (is_export, is_print) {

        var param = {
            is_export: is_export,
            is_print: is_print,
            bulan: moment($scope.filter.bulan).format('MMMM YYYY'),
        };


        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/laporan', param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.data = response.data.data;
                    $scope.detail = response.data.detail;
                    $scope.tampilkan = true;
                } else {
                    $scope.tampilkan = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "l_penjualanbarang/view?" + $.param(param), "_blank");
            });
        }
    };

});