app.controller("mbarangCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("m_barang/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            console.log($scope.displayed);
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.cariKategori = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('m_barang/kategori', {'nama': query}).then(function (response) {
                $scope.listBarang = response.data.list;
            });
        }
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama_barang;
        $scope.form = form;
        // $scope.getFoto(form.id);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama_barang;
        $scope.form = form;
        // $scope.getFoto(form.id);
    };
    $scope.save = function (form) {
        console.log(form);
        console.log("ebebbe");
        $scope.loading = true;
        Data.post("m_barang/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post("m_barang/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post("m_barang/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    //Fungsi Upload Foto
    // $scope.getFoto = function (id) {
    //     var id = id;
    //     console.log("id", id);
    //     Data.get("m_barang/getFoto", {'m_barang_id': id}).then(function (response) {
    //         $scope.listFoto = response.data;
    //     });
    // };
    //
    // $scope.remFoto = function(val) {
    //     Data.post("m_barang" + "/hapusFoto", val).then(function(response) {
    //         if (response.status_code == 200) {
    //             $rootScope.alert("Berhasil", "Foto berhasil disimpan", "success");
    //             // toaster.pop("success", "Berhasil", "Foto berhasil dihapus");
    //             $scope.listFoto.splice($scope.listFoto.indexOf(val), 1);
    //         }
    //     });
    // };
    //
    // var uploader = (
    //     $scope.uploader = new FileUploader({
    //         url: Data.base + "m_barang" + "/upload",
    //         formData: [],
    //         removeAfterUpload: true
    //
    //     })
    // );
    // console.log("asbf", uploader);
    // $scope.uploadFile = function (form) {
    //     console.log("sdbfh", form);
    //     if ($scope.form.id == undefined) {
    //         $scope.save(0);
    //     } else {
    //         $scope.uploader.uploadAll();
    //     }
    // };
    // uploader.filters.push({
    //     name: "fileFilter",
    //     fn: function (item) {
    //         var type = "|" + item.type.slice(item.type.lastIndexOf("/") + 1) + "|";
    //         var x = "|jpg|JPG|png|PNG|jpeg|".indexOf(type) !== -1;
    //         if (!x) {
    //             toaster.pop("error", "Jenis File tidak sesuai");
    //         }
    //         return x;
    //     }
    // });
    // uploader.filters.push({
    //     name: "sizeFilter",
    //     fn: function (item) {
    //         var xz = item.size < 2097152;
    //         if (!xz) {
    //             toaster.pop("error", "Ukuran gambar tidak boleh lebih dari 2 MB");
    //         }
    //         return xz;
    //     }
    // });
    // uploader.onSuccessItem = function (fileItem, response) {
    //     if (response.status_code == 200) {
    //         $scope.getFoto(response.data.m_barang_id);
    //     }
    // };
    // uploader.onBeforeUploadItem = function (item) {
    //     item.formData.push({
    //         id: $scope.form.id
    //     });
    // };
});
