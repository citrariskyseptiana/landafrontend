app.controller("tpembelianCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_pembelian/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    $scope.getDetail = function (id) {
        Data.get("t_pembelian/view?t_pembelian_id=" + id).then(function (response) {
            $scope.listDetail = response.data;
        });
    };
    $scope.listDetail = [{}];
    $scope.getDetail = function (form) {
        console.log('test')
        console.log(form);
        Data.get("t_pembelian/view/" + form).then(function (data) {
            $scope.listDetail = data.data;
            console.log(data);
            $scope.getSubTotal();
        });
    }

    $scope.addDetail = function (listDetail, barang = undefined) {
        // var comArr = $scope.listDetail.length;
        var newDet = {};
        if(barang != undefined){
            newDet = barang;
        }
        $scope.listDetail.push(newDet);
    };
    $scope.removeDetail = function (val, paramindex) {
        var comArr = eval(val);
        if (comArr.length > 1) {
            val.splice(paramindex, 1);
        } else {
            alert("Something gone wrong");
        }
    };
    $scope.cariSupplier = function (query) { //untuk mencari supplier pada ui select
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_pembelian/supplier', {'nama_supplier': query}).then(function (response) {
                $scope.listSupplier = response.data.list;
            });
        }
    };
    $scope.cariBarang = function (query) { // untuk mencari supplier pada ui select
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_pembelian/barang', {'nama_barang': query}).then(function (response) {
                $scope.listBarang = response.data.list;
            });
        }
    };
    $scope.getSubTotal = function () { //fungsi yang ketika input jumlah dan harga subtotal dan total akan otomatis terisi
        var total = 0;
        angular.forEach($scope.listDetail, function (value, key) {
            value.SubTotal = value.jumlah * value.harga;
            total = total + value.SubTotal;
        });
        $scope.form.total = total;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [{}]; //untuk mengosongkan form ketika create
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.tanggal;
        $scope.form = form;
        $scope.getDetail(form.id);
        $scope.form.tanggal = new Date(form.tanggal); //set tanggal yang sudah tersimpan
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.tanggal;
        $scope.form = form;
        $scope.getDetail(form.id);
        $scope.form.tanggal = new Date(form.tanggal);
    };
    $scope.save = function (form, status) {
        $scope.loading = true;
        form.status = status;
        var params = {
            data: form,
            detail: $scope.listDetail
        }
        Data.post("t_pembelian/save", params).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            row.is_deleted = 0;
            Data.post("t_pembelian/hapus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.ModalTambahBarang = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_pembelian/ModalTambahBarang.html",
            controller: "barangCtrl",
            size: "md",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: {},
            }
        });
        modalInstance.result.then(function (response) {
//            console.log(response)
            if (response.data == undefined) {
            } else {
                var newDet = {
                    m_barang_id: response.data
                }
                $scope.addDetail($scope.listBarang, newDet);
            }
        });
    }
    $scope.ModalSupplier = function (m_supplier_id) {
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/t_pembelian/ModalSupplier.html",
            controller: "supplierCtrl",
            size: "md",
            backdrop: "static",
            keyboard: false,
            resolve: {
                form: {
                    m_supplier_id : m_supplier_id
                },
            }
        });
        console.log("jjl", $scope.form.m_supplier_id)
        modalInstance.result.then(function (response) {
//            console.log(response)
            if (response.data == undefined) {
            } else {
                $scope.addDetail({
                    no: '',
                    m_supplier_id:response.data,
                    telepon:response.data.telepon,
                    alamat:response.data.alamat
                });
            }
        });
    }
});
app.controller("supplierCtrl", function ($scope, Data, $uibModalInstance, $rootScope, form) {
    console.log(form);
    $scope.form = {};
    $scope.form = form.m_supplier_id != undefined ? form.m_supplier_id : form;
    // Data.get('m_kategori/index', {filter: {"m_kategori.is_deleted": 0}}).then(function (response) {
    //     $scope.listBarang = response.data.list;
    // });

    $scope.save = function (form) {
        Data.post('m_supplier/save', form).then(function (result) {
//            console.log(result)
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
//                console.log(form)
                $uibModalInstance.close({
                    'data': result.data
                });
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });

    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});
app.controller("barangCtrl", function ($scope, Data, $uibModalInstance, $rootScope, form) {

    $scope.form = {};
    Data.get('m_kategori/index', {filter: {"m_kategori.is_deleted": 0}}).then(function (response) {
        $scope.listBarang = response.data.list;
    });

    $scope.save = function (form) {
        Data.post('m_barang/save', form).then(function (result) {
//            console.log(result)
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
//                console.log(form)
                $uibModalInstance.close({
                    'data': result.data
                });
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
        });

    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});
