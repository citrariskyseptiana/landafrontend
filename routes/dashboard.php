<?php
$app->get("/dashboard/barChart", function ($request, $response){
        $params = $request->getParams();
        $db = $this->db;
    
        $db->select("
            m_barang.nama_barang AS barang_nama,
            SUM(t_penjualan_det.jumlah) AS penjumlahan")
            ->from("t_penjualan")
            ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
            ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
            ->where("t_penjualan.status", "=", "tersimpan")
            ->groupby("m_barang_id")
            ->orderby("penjumlahan DESC");

            $nama_barang = [];
            $jumlah_barang = [];

            $models = $db->findAll();

            foreach($models as $key => $value){
                array_push($nama_barang, $value->barang_nama);
                array_push($jumlah_barang, $value->penjumlahan);
            }

            $jumlah_barang = array_map('intval', $jumlah_barang);
            array_multisort($jumlah_barang, SORT_ASC, $nama_barang, SORT_DESC, $models);

        return successResponse($response, ["nama_barang" => $nama_barang, "jumlah_barang"=>$jumlah_barang]);
});

$app->get("/dashboard/lineChart", function ($request, $response){
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_barang.*,
        m_barang.nama_barang AS barang_nama,
        m_kategori.nama AS nama_kategori,
        SUM(t_penjualan_det.jumlah) AS penjumlahan,
        MONTH(t_penjualan.tanggal) AS tanggal_penjualan")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id=m_kategori.id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->groupby("m_kategori.id", "tanggal_penjualan")
        ->orderby("tanggal_penjualan DESC");

        $models = $db->findAll();
        // print_r($models);
        // die;

        $month = range(1, 12);
        $kat = [];
        $res = [];

        foreach($models as $key => $value){
            $res[$value->nama_kategori][$value->tanggal_penjualan] = intval($value->penjumlahan);
        }
//         print_r($res);
//         die;

        $series = [];
        foreach ($res as $key => $value) {
            $new = [];
            foreach($month as $ke => $val){
                if(!isset($res[$key][$val])){
                    $res[$key][$val] = 0;
                }
            }
            ksort($res[$key]);
            $new = [
                'values' => array_values($res[$key]),
                'text' => $key,
            ];
            $series[] = $new;
        }
//         print_r($);
//         die;

    return successResponse($response, ["nama_kategori" => $kat, "result"=>$res, "series"=>$series]);
});