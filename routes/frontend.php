<?php
$app->get('/home', function ($request, $response) {
    return $this->view->render($response, 'frontend/home.twig', [
        'page' => 'home',
        'keyword' => 'keyword',
        'description' => 'Deskripsi web',
    ]);
});
$app->get('/about', function ($request, $response) {
    return $this->view->render($response, 'frontend/about.twig', [
        'page' => 'about'
    ]);
});
$app->get('/contact', function ($request, $response) {
    return $this->view->render($response, 'frontend/contact.twig', [
        'page' => 'contact',
    ]);
});
$app->get('/detail', function ($request, $response) {
    $db = $this->db;
    $params = $_GET;

    $db->select("
        m_barang.id, m_barang.nama_barang, m_barang.deskripsi")
        ->from("m_barang")
        ->where("id", "like", $params["index"]);

    $models = $db->find();
//    print_r($models);
//    die;

    return $this->view->render($response, 'frontend/detail.twig', [
        'page' => 'detail',
        'data' => $models,
    ]);
});
$app->get('/shop', function ($request, $response) {

    $db = $this->db;
    $page = isset($_GET['page']) && $_GET['page'] != "" ? $_GET['page'] : 1;
    $hasil = createPagination('m_barang', 2, 6, $page, config('SITE_URL') . "/shop");

//    $img_path = __DIR__;
//    $img_path = substr($img_path, 0, strpos($img_path, "routes")) . "file/barang/";

    $db->select("
        m_barang.id, m_barang.nama_barang, m_barang.harga, m_barang_img.foto")
        ->from("m_barang_img")
        ->join("left join", "m_barang", "m_barang_img.m_barang_id=m_barang.id")
        ->groupby("m_barang.id");

    $models = $db->findAll();
//    print_r($hasil);
//    die;
    return $this->view->render($response, 'frontend/shop.twig', [
        'page' => 'shop',
        'data' => $hasil['resultData'],
        'pagination' => $hasil['pagination'],
    ]);
});

//if(isset($_POST['submit'])){
//    echo "nama: ".$_POST['nama']."<br/>";
//    echo "email: ".$_POST['email']."<br/>";
//    echo "pesan: ".$_POST['pesan']."<br/>";
//    echo "subject: ".$_POST['subject']."<br/>";
//}
//print_r($_POST);
//die;


$app->post("/submitcontact", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $submit = $_POST;

    $db->insert("m_contact", $submit);

    return $this->view->render($response, 'frontend/submitcontact.twig', [
        'page' => 'submitcontact'
    ]);
});

$app->get('/blog', function ($request, $response) {
    $db = $this->db;
//    SUBSTR(m_artikel.isi_artikel, 1, 50) AS artikel,
    $db->select("
        m_artikel.id, m_artikel.judul, m_artikel.publish, m_artikel.deskripsi, m_artikel.url,
        m_artikel.isi_artikel AS artikel,
        m_kategori_artikel_id, m_kategori_artikel.nama")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id");

    $models = $db->findAll();
    $newsList   = [];
    $defaultImg = config("SITE_IMG") . "no-image.png";
    foreach ($models as $key => $value){
        $newsList[$key]                 = (array)$value;
        $firstImg                       = get_images($value->artikel);
        $newsList[$key]['firstImg']     = isset($firstImg[0]) ? $firstImg[0] : $defaultImg;
        $newsList[$key]['url']          = config('SITE_URL') . "frontend/blog/" . $value->url;
        $newsList[$key]['description']  = !empty($value->deskripsi);
    }
//    print_r($newsList);
//    die;

    $db->select("
    m_kategori_artikel.id, m_kategori_artikel.nama,
    COUNT(m_kategori_artikel.id) AS jumlah")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id")
        ->groupby("m_kategori_artikel.id");
    $result = $db->findAll();

    $db->select("
        m_artikel.id, m_artikel.judul, m_artikel.isi_artikel, m_artikel.publish,
        m_kategori_artikel_id, m_kategori_artikel.nama")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id")
        ->orderby("m_artikel.publish DESC")
        ->limit(4);

    $recent = $db->findAll();
//    print_r($recent);
//    die;
    date_default_timezone_set('Asia/Jakarta');
    $hariini = strtotime("now");
    foreach ($recent as $key => $value){
        $besok = strtotime($recent[$key]->publish);
        $detik = strtotime('1 day 30 second', 0);

        if(($hariini - $besok) <= $detik){
            $delta = $hariini - $besok;
            $format1 = new DateTime("@0");
            $format2 = new DateTime("@$delta");
            $waktujam = $format1->diff($format2)->format('%h');
//            print_r($waktujam);
//            die;

            $recent[$key]->publish = $waktujam;
            $recent[$key]->type = "hour";
        }
        else{
            $recent[$key]->type = "day";
        }
    }
//    print_r($recent);
//    die;
    return $this->view->render($response, 'frontend/blog.twig', [
        'page' => 'blog',
        'kategori' => $result,
        'art' => $newsList,
        'recenpost' => $recent,
    ]);
});

$app->get('/detailblog', function ($request, $response) {
    $db = $this->db;
    $params = $_GET;

    $db->select("
        m_artikel.id, m_artikel.judul, m_artikel.isi_artikel")
        ->from("m_artikel")
        ->where("id", "like", $params['index']);

    $models = $db->find();
//    print_r($models);
//    die;

    return $this->view->render($response, 'frontend/detailblog.twig', [
        'page' => 'detailblog',
        'detail' => $models,
    ]);
});

