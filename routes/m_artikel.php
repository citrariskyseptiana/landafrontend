<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "m_kategori_artikel_id" => "required",
        "judul" => "required",
        "isi_artikel" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_artikel/kategori", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kategori_artikel")
        ->where("nama", "like", $params["nama"]);

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});
/**
 * Ambil semua m artikel
 */
$app->get("/m_artikel/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_artikel.*,
    m_kategori_artikel.nama AS kategori_artikel")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
//print_r($models);
//die;
    foreach ($models as $key => $val){
        $models[$key]->m_kategori_artikel_id = [
            "id" => $val->m_kategori_artikel_id,
            "nama"=> $val->kategori_artikel
        ];
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m artikel
 */
$app->post("/m_artikel/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "routes")) . "img/artikel/";
    date_default_timezone_set("Asia/Jakarta");

    $validasi = validasi($data);
    if ($validasi !== true)
        return unprocessResponse($response, $validasi);

    try {
        $data['m_kategori_artikel_id'] = $data['m_kategori_artikel_id']['id'];
        $data["publish"] = date("Y-m-d H:i:s", strtotime($data["publish"]));

        // Proses upload gambar ke server
        $images = get_images($data["isi_artikel"]);
//        print_r($images);
//        die;

        foreach ($images as $key => $value) {

            if (is_base64($value)) {
                $img_file_name = base64toImg($value, $file_path);
                $img_url = config("SITE_IMG") . 'artikel/' . $img_file_name['data'];
                $data['isi_artikel'] = str_replace($value, $img_url, $data['isi_artikel']);
            }
        }
        // End Proses upload gambar ke server
        // Cek apakah url alias unik
        $db->select("url")
            ->from("m_artikel")
            ->where('url', '=', $data['url'])
            ->andwhere('is_deleted', '=', 0);
        if (isset($data['id'])) {
            $db->andwhere('id', '<>', $data['id']);
        }
        $periksa_alias = $db->find();

        if ($periksa_alias)
            return unprocessResponse($response, ['URL artikel sudah dipakai, silakan gunakan URL yg baru.']);
        // End of Cek apakah url alias unik

        if (isset($data["id"])) {
            $model = $db->update("m_artikel", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("m_artikel", $data);
        }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
/**
 * Hapus m artikel
 */
$app->post("/m_artikel/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi == true) {
        try {
            $model = $db->update("m_artikel", ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
