<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "m_kategori_id" => "required",
        "nama_barang" => "required",
        "stok_barang" => "required",
        "satuan" => "required",
        "harga" => "required",
        "deskripsi" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m barang
 */
$app->get("/m_barang/kategori", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kategori")
        ->where("nama", "like", $params["nama"]);

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});

$app->get("/m_barang/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_barang.*,
                m_kategori.nama AS kategori_nama")
        ->from("m_barang")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id=m_kategori.id");
//        ->join("left join", "m_barang_img", "m_barang_img.m_barang_id=m_barang.id");
//        ->where("is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
//    print_r($models);
//    die;
    foreach ($models as $key => $value) {
//        print_r($value);
//        die;
        $models[$key]->m_kategori_id = [
            "id" => $value->m_kategori_id,
            "nama" => $value->kategori_nama
        ];
//        print_r($models);
//        die;
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m barang
 */
$app->post("/m_barang/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "routes")) . "file/barang/";
    $validasi = validasi($data);

    if ($validasi === true) {
        try {

            $data['m_kategori_id'] = (isset($data['m_kategori_id']['id'])) ? $data['m_kategori_id']['id'] : null;
//            $data['m_kategori_id']['id']
            if (isset($data["id"])) {
                $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_barang", $data);
            }
            if (!empty($data["foto"])) {
                foreach ($data["foto"] as $key => $val) {
                    $riwfile = $db->find("SELECT id FROM m_barang_img ORDER BY id DESC");
                    $gid = (isset($riwfile->id)) ? $riwfile->id + 1 : 1;
                    $val["filename"] = $gid . "_" . $val["filename"];
                    $simpan_file = base64ToFile($val, $file_path);

                    //Cara ke Dua
                    $temp = explode(".", $simpan_file["fileName"]);
                    $newfilename = $key . '_' . $model->id . '_' . $model->nama_barang . '.' . end($temp);
                    rename($simpan_file['filePath'], $file_path . $newfilename);

                    $val["m_barang_id"] = $model->id;
                    $val["foto"] = $val["filename"];
//                    print_r($val);
//                    die;
                    $model = $db->insert("m_barang_img", $val);
//                    print_r($model);
//                    die;
                }
            }
//            print_r($model);
//            die;
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m barang
 */
$app->post("/m_barang/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

//Fungsi Get Foto
//$app->get("/m_barang/getFoto", function ($request, $response) {
//    $db = $this->db;
//    $params = $request->getParams();
//
//        $listFoto = $db->select("*")
//            ->from("m_barang_img")
//            ->where("m_barang_id", "=", $params['m_barang_id'])
//            ->findAll();
////        print_r($listFoto);
////        die;
//
//        return successResponse($response, $listFoto);
//
//});
//$app->post('/m_barang/upload', function ($request, $response) {
//    $sql          = $this->db;
//    $data         = $request->getParams();
//    $data['file'] = isset($_FILES['file']) ? $_FILES['file'] : '';
//    $img_path     = __DIR__;
//    $img_path     = substr($img_path, 0, strpos($img_path, "routes")) . "file/barang/";
////    print_r($data);
////    die;
//
//    if (!empty($data['file'])) {
//        $tempPath = $data['file']['tmp_name'];
//        $riwfile  = $sql->find("SELECT id FROM m_barang_img ORDER BY id DESC");
//        $gid      = (isset($riwfile->id)) ? $riwfile->id + 1 : 1;
//        $newName  = $gid . "_" . $data['file']['name'];
//
//        $folder     = $img_path . $data['id'];
//        $uploadPath = $folder . DIRECTORY_SEPARATOR . $newName;
//
//        if (!is_dir($folder)) {
//            mkdir($folder, 0777, true);
//        }
//
//        move_uploaded_file($tempPath, $uploadPath);
//        $file = $uploadPath;
//        if (file_exists($file)) {
//            $answer = [
//                'answer'      => 'File transfer completed',
//                'foto'        => $newName,
//                'id'          => $gid,
//                'm_barang_id' => $data['id'],
//            ];
//
//            $data = [
//                'id'          => $gid,
//                'm_barang_id' => $data['id'],
//                'foto'        => $newName,
//            ];
//            $create_file = $sql->insert('m_barang_img', $data);
//
//            return successResponse($response, $answer);
//        }
//    } else {
//        echo 'No files';
//    }
//});
//$app->post('/m_barang/hapusFoto', function ($request, $response) {
//    $db   = $this->db;
//    $data = $request->getParams();
//
//    try {
//        $hapusFoto = $db->delete("m_barang_img", ['id' => $data['id']]);
//
//        // Hapus file
//        $img_path = __DIR__;
//        $img_path = substr($img_path, 0, strpos($img_path, "routes")) . "file/barang/";
//        unlink($img_path . $data["m_barang_id"] . "/" . $data["foto"]);
//
//        return successResponse($response, ['Berhasil menghapus foto!']);
//    } catch (Exception $e) {
//        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
//    }
//});
