<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "tanggal" =>"required",
        "total" => "required",
        "m_supplier_id" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil detail t pembelian
 */
$app->get("/t_pembelian/supplier", function ($request, $response) { //untuk menampilkan pilihan di ui select
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_supplier")->where("nama_supplier", "like", $params["nama_supplier"]);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});
$app->get("/t_pembelian/barang", function ($request, $response) { //untuk menampilkan pilihan di ui select
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_barang")->where("nama_barang", "like", $params["nama_barang"]);

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get("/t_pembelian/view/{id}", function ($request, $response) { //fungsi melihat data pada form ketika dipilih (di klik 2kali pada salah satu form/detail)
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("t_pembelian_det.*, m_barang.nama_barang")
        ->from("t_pembelian_det")
        ->join("left join", "m_barang", "m_barang.id=t_pembelian_det.m_barang_id")
        ->where("t_pembelian_id", "=", $id);
    $models = $db->findAll();
    foreach ($models as $key => $value) { //format data mengubah array menjadi string
        $models[$key]->m_barang_id = [
            "id" => $value->m_barang_id,
            "nama_barang" => $value->nama_barang
        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua t pembelian
 */
$app->get("/t_pembelian/index", function ($request, $response) { // menampilkan data pada index
    $params = $request->getParams();
    $db = $this->db;
    $db->select(" 
    t_pembelian.*,
    m_supplier.nama_supplier AS supplier_nama, m_supplier.no_telepon AS telepon, m_supplier.alamat_supplier AS alamat")
        ->from("t_pembelian")
        ->join("left join", "t_pembelian_det", "t_pembelian_det.t_pembelian_id=t_pembelian.id")
        ->join("left join", "m_supplier", "t_pembelian.m_supplier_id=m_supplier.id")
        ->join("left join", "m_barang", "t_pembelian_det.m_barang_id=m_barang.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key => $value) { //untuk format data array menjadi string
//        print_r($value);
//        die;
        $models[$key]->m_supplier_id = [
            "id" => $value->m_supplier_id,
            "nama_supplier" => $value->supplier_nama,
            "no_telepon" => $value->telepon,
            "alamat_supplier" => $value->alamat
        ];
    }
//    print_r($models);
//    die;
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t pembelian
 */
$app->post("/t_pembelian/save", function ($request, $response) { //untuk fungsi menyimpan
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            $data['data']['tanggal'] = date("Y-m-d", strtotime($data['data']['tanggal']));
            $data['data']['m_supplier_id'] = $data['data']['m_supplier_id']['id'];
            if (isset($data["data"]["id"])) {
                $model = $db->update("t_pembelian", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_pembelian_det", ["t_pembelian_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("t_pembelian", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if (isset($data["detail"]) && !empty($data["detail"])) { //fungsi menyimpan data array menjadi string pada detail
                foreach ($data["detail"] as $key => $val) {
                    $detail["m_barang_id"] = $val["m_barang_id"]["id"];
                    $detail["jumlah"] = $val["jumlah"];
                    $detail["harga"] = $val["harga"];
                    $detail["t_pembelian_id"] = $model->id;
//                    print_r($detail);
//                    die;
                    $db->insert("t_pembelian_det", $detail);
                    if ($model->status == "tersimpan") { //untuk menyimpan pada tabel t_pembelian_det dan mengupdate stok otomatis ke m_barang
                        $db->run("UPDATE m_barang SET stok_barang=stok_barang+" . $val["jumlah"] . " WHERE id = " . $val["m_barang_id"]["id"]);
                    }
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t pembelian
 */
$app->post("/t_pembelian/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_pembelian", ["id" => $data["id"]]);
        $modelDetail = $db->delete("t_pembelian_det", ["t_pembelian_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
