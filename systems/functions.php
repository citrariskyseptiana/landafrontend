<?php
function twigView()
{
    $view = new \Slim\Views\Twig('views');
    return $view;
}

function createPagination($table_name, $_adjacents, $_limit, $_page, $_url)
{
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    /* How many records you want to show in a single page. */
    $limit = $_limit;

    /* How may adjacent page links should be shown on each side of the current page link. */
    $adjacents = $_adjacents;

    /* Get total number of records */
    if ($table_name == 'm_barang') {
        $row = $db->find("SELECT COUNT(id) 'total_rows'
                        FROM $table_name
                        WHERE is_deleted=0
                        ");
    } else {
        $row = $db->find("SELECT COUNT(id) FROM $table_name WHERE is_deleted=0");
    }

    $total_rows = $row->total_rows;
//    print_r($total_rows);
//    die;
    /* Get the total number of pages. */
    $total_pages = ceil($total_rows / $limit);

    $page = $_page;
    $offset = $limit * ($page - 1);

    if ($table_name == 'm_barang') {
        $resultData = $db->select("
            m_barang_img.foto,
            m_barang.id, m_barang.nama_barang, m_barang.harga, m_barang.deskripsi
        ")
            ->from("m_barang_img")
            ->join("LEFT JOIN", "m_barang", "m_barang_img.m_barang_id = m_barang.id")
            ->groupBy("m_barang.id")
            ->offset($offset)
            ->limit($limit)
            ->findAll();
//        print_r($l);
//        die;
    }

    //Checking if the adjacent plus current page number is less than the total page number.
    //If small then page link start showing from page 1 to upto last page.
    if ($total_pages <= (1 + ($adjacents * 2))) {
        $start = 1;
        $end = $total_pages;
    } else {
        if (($page - $adjacents) > 1) {
            //Checking if the current page minus adjacent is greateer than one.
            if (($page + $adjacents) < $total_pages) {  //Checking if current page plus adjacents is less than total pages.
                $start = ($page - $adjacents);         //If true, then we will substract and add adjacent from and to the current page number
                $end = ($page + $adjacents);         //to get the range of the page numbers which will be display in the pagination.
            } else {                                   //If current page plus adjacents is greater than total pages.
                $start = ($total_pages - (1 + ($adjacents * 2)));  //then the page range will start from total pages minus 1+($adjacents*2)
                $end = $total_pages;                           //and the end will be the last page number that is total pages number.
            }
        } else {                                       //If the current page minus adjacent is less than one.
            $start = 1;                                //then start will be start from page number 1
            $end = (1 + ($adjacents * 2));             //and end will be the (1+($adjacents * 2)).
        }
    }

    // -- Pagination Bootstrap -- //
    if ($total_pages > 1) {
        $pagination = '';

        // Link of the first page
        $pagination .= "<ul class='pagination pagination-sm justify-content-center'>
								<li class='page-item " . ($page <= 1 ? 'disabled' : '') . "'>
									<a class='page-link' href=" . $_url . "?page=1" . ">&lt;&lt;</a>
								</li>";

        // Link of the previous page
        $pagination .= "<li class='page-item " . ($page <= 1 ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . ($page > 1 ? $page - 1 : 1) . ">&lt;</a>
							</li>";

        // Links of the pages with page number
        for ($i = $start; $i <= $end; $i++) {
            $pagination .= "<li class='page-item " . ($i == $page ? 'active' : '') . "'>
									<a class='page-link' href=" . $_url . "?page=" . $i . ">" . $i . "</a>
								</li>";
        }

        // Link of the next page
        $pagination .= "<li class='page-item " . ($page >= $total_pages ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . ($page < $total_pages ? $page + 1 : $total_pages) . ">&gt;</a>
							</li>";

        // Link of the last page
        $pagination .= "<li class='page-item " . ($page >= $total_pages ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . $total_pages . ">&gt;&gt;</a>
							</li>
						</ul>";
    }

    // -- End of Pagination Bootstrap -- //
//    print_r($resultData);
//    die;
    return [
        'resultData' => $resultData,
        'pagination' => $pagination,
    ];

}
function get_images($html) {
    preg_match_all('@src="([^"]+)"@', $html, $match);
    $src = array_pop($match);
    return $src;
}

function is_base64($base64) {
    $data = $base64;
    $data = str_replace(" ", "+", $data);
    $data = str_replace(":=", "==", $data);

    $cekData = sizeof(explode(';', $data));
    if ($cekData <2){
        return false;
    }
    list($type, $data) = explode(';', $data);

    list(, $data) = explode(',', $data);
    list($header, $ext) = explode("/", $type);

    if (isset($data) and base64_decode($data)) {
        return true;
    } else {
        return false;
    }
}

function base64toImg($base64, $path, $name = null) {
    $data = $base64;
    $data = str_replace(" ", "+", $data);
    $data = str_replace(":=", "==", $data);

    if (!empty($data) and is_base64($data)) {
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        list($header, $ext) = explode("/", $type);

        $data = base64_decode($data);
        if (empty($name)) {
            $name = date("ymdis");
        }

        $allowExt = array('PNG', 'JPG', 'jpg', 'png', 'JPEG', 'jpeg');
        if (in_array($ext, $allowExt)) {
            $fileName = $name . '.' . $ext;
            file_put_contents($path . $fileName, $data);
            return [
                'status' => true,
                'data' => $fileName,
            ];
        } else {
            return [
                'status' => false,
                'data' => ['ekstensi gambar harus JPG atau PNG'],
            ];
        }
    } else {
        return [
            'status' => false,
            'data' => $data,
        ];
    }
}
